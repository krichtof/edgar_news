var App = Ember.Application.create({
	LOG_TRANSITIONS: true
});
// App.ApplicationAdapter = DS.FixtureAdapter.extend();
// App.ApplicationAdapter = DS.RESTAdapter.extend();
App.Router.map(function() {
	this.route('about');
	this.resource('articles');
})
App.ArticlesRoute = Ember.Route.extend({
	model: function() {
		return this.store.findAll('article');
	}
})

App.ArticlesController = Ember.ArrayController.extend({
	itemController: 'article',
	addingNewArticle: false,
	actions: {
		newArticle: function() {
			console.log('addArticle');
			this.set('addingNewArticle', true)
		},
		addArticle: function() {
			var article = this.store.createRecord('article', {
				title: this.get('title'),
				description: this.get('description'),
				link: this.get('link')
			});
			article.save();
			this.set('addingNewArticle', false);
			this.set('title','');
			this.set('description','');
			this.set('link','');
		}
	}
});
App.ArticleController = Ember.ObjectController.extend({
	isEditing: false,
	actions: {
		updateArticle: function() {
			console.log('updating article #' + this.get('id'));
			article = this.get('model');
			if (article.get('isDirty') ){ 
				article.save(); 
			}
			this.set('isEditing', false)
		},
		editArticle: function() {
			this.set('isEditing', true)
		},
		deleteArticle: function(){
			article = this.get('model');
			article.destroyRecord();
		}
	}
})

App.Article = DS.Model.extend({
	title: DS.attr('string'),
	description: DS.attr('string'),
	link: DS.attr('string')
})
App.Article.FIXTURES = [
  {
		id: 1,
		title: "Edgar ENTRETIEN",
		description: "Edgar reçoit San Severino",
		link: "http://vimeo.com/24424"
	},
	{
		id: 2,
		title: "L'industrie musicale en question",
		description: "Tous les chiffres de l'étude",
		link: "http://irma.com/24424"
	}
]

