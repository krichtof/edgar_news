json.articles do
  json.extract! @article, :id, :title, :description, :link, :created_at, :updated_at
end